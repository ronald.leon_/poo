﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guia5
{
    public partial class Notas : Form
    {
        public Notas()
        {
            InitializeComponent();
        }

        //Patron de validación para números con expresión regular
        Regex regexNumero = new Regex(@"^[0.0-9.9]+$");

        //Notas por periodo
        decimal notasPeriodo1;
        decimal notasPeriodo2;
        decimal notasPeriodo3;
        decimal promedio;

        //Las validaciones son tomadas mediante las propiedades del numericUpDown los cuales me permiten
        //ubicar un valor minimo de 0 y máximo de 10 y siendo estos números tanto enteros como decimales
        //

        private void btnSiguienteP1_Click(object sender, EventArgs e)
        {
            //Sumo las notas del Periodo 1 y paso al siguiente tabControl
            notasPeriodo1 = Convert.ToDecimal(nudNota1P1.Value + nudNota2P1.Value + nudNota3P1.Value);
            MenuControl.SelectedTab = tabPage2;
        }

        private void btnSiguienteP2_Click(object sender, EventArgs e)
        {
            //Sumo las notas del Periodo 2 y paso al siguiente tabControl
            notasPeriodo2 = Convert.ToDecimal(nudNota1P2.Value + nudNota2P2.Value + nudNota3P2.Value);
            MenuControl.SelectedTab = tabPage3;
        }

        private void btnSiguienteP3_Click(object sender, EventArgs e)
        {
            //Sumo las notas del Periodo 3
            notasPeriodo3 = Convert.ToDecimal(nudNota1P3.Value + nudNota2P3.Value + nudNota3P3.Value);

            //Calculo promedio de los 3 periodos de la materia redondeado a dos decimales
            promedio = Math.Round((notasPeriodo1 + notasPeriodo2 + notasPeriodo3) / 3, 2);

            //Muestro resultado
            MessageBox.Show("Su promedio de notas es: " + promedio.ToString());
        }
    
    }
}
