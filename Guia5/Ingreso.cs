﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guia5
{
    public partial class Ingreso : Form
    {
        public Ingreso()
        {
            InitializeComponent();
        }

        private void btnContinuar_Click(object sender, EventArgs e)
        {
            //Creo objeto de clase
            Estudiante miEstudiante = new Estudiante();

            DateTime fechaNacimiento = dtpFechaNac.Value;
            // verificamos comparando con la fecha del sistema
            int anio = System.DateTime.Now.Year - fechaNacimiento.Year;
            int mes = System.DateTime.Now.Month - fechaNacimiento.Month;
            int dia = System.DateTime.Now.Day - fechaNacimiento.Day;

            /*verificamos si su fecha de nacimiento es hoy ó en el futuro*/
            if (anio == 0 && mes <= 0 && dia <= 0)
            {
                MessageBox.Show("Fecha invalida");
                return;
            }
            else
            {
                try
                {
                    //guardos los datos en sus respectivos atributos de la clase
                    miEstudiante.Carnet = txtCarnet.Text;
                    miEstudiante.Nombre = txtNombre.Text;
                    miEstudiante.FechaNac = dtpFechaNac.Value;
                    miEstudiante.Correo = txtCorreo.Text;
                    miEstudiante.Responsable = txtResponsable.Text;
                }
                catch (Exception x)
                {
                    //En caso de error lo muestro y reinicio los campos 
                    MessageBox.Show(x.Message);
                    txtCarnet.Text = "";
                    txtNombre.Text = "";
                    dtpFechaNac.Value = DateTime.Now;
                    txtCorreo.Text = "";
                    txtResponsable.Text = "";
                    return;
                }               
            }          

            //En caso de exito, muestro mensaje y paso al siguiente form de notas
            MessageBox.Show("Exito, pasaremos al siguiente formulario");
            Notas Notas = new Notas();
            Notas.Show();
        }
    }
}
