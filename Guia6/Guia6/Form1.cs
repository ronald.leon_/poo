﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guia6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Guia5.MenuEjercicios menuEjercicios = new Guia5.MenuEjercicios();
            menuEjercicios.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Guia4.G4_Menu_Ejercicios menu_Ejercicios = new Guia4.G4_Menu_Ejercicios();
            menu_Ejercicios.Show();
        }
    }
}
