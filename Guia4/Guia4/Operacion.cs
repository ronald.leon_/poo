﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guia4
{
    class Operacion
    {
        protected int valor1;

        public int Valor1
        {
            get { return valor1; }
            set { valor1 = value; }
        }

        protected int valor2;

        public int Valor2
        {
            get { return valor2; }
            set { valor2 = value; }
        }

        protected int resultado;

        public int Resultado
        {
            get { return resultado; }
            set { resultado = value; }
        }

    }

    class Sumar: Operacion //herencia de la clase operacion
    {
        public int operar(int v1, int v2)
        {
            valor1 = v1; //se puede acceder a los atributos directamente
            valor2 = v2; //por la relacion de herencia
            return resultado = valor1 + valor2;
        }
    }

    class Restar: Operacion //herencia de la clase operacion
    {
        public int operar(int v1, int v2)
        {
            valor1 = v1; //se puede acceder a los atributos directamente
            valor2 = v2; //por la relacion de herencia
            return resultado = valor1 - valor2;
        }
    }

    class Multiplicar: Operacion //herencia de la clase operacion
    {
        public int operar(int v1, int v2)
        {
            valor1 = v1; //se puede acceder a los atributos directamente
            valor2 = v2; //por la relacion de herencia
            return resultado = valor1 * valor2;
        }
    }
}
