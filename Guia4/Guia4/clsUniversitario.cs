﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guia4
{
    public class clsUniversitario : clsEstudiante
    {
        private string nombreUniversidad;//atributo
        public string NombreUniversidad //propiedad
        {
            get { return nombreUniversidad; }
            set { this.nombreUniversidad = value; }
        }

        private string carrera;//atributo
        public string Carrera //propiedad
        {
            get { return carrera; }
            set { this.carrera = value; }
        }

        private int materiasInscritas;//atributo
        public int MateriasInscritas //propiedad
        {
            get { return materiasInscritas; }
            set { this.materiasInscritas = value; }
        }

        private double notas;//atributo
        public double Notas //propiedad
        {
            get { return notas; }
            set { this.notas = value; }
        }

        private double cum;//atributo
        public double CUM //propiedad
        {
            get { return cum; }
            set { this.cum = value; }
        }

        //Constructor
        public clsUniversitario()
        {
            this.nombreUniversidad = "";
            this.carrera = "";
            this.materiasInscritas = 0;
            this.notas = 0;
            this.cum = 0;
        }
    }
}
