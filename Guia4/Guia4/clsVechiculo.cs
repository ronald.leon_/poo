﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Guia4
{
    public class clsVehiculo //Clase Base
    {
        //Patron de validación para números con expresión regular
        Regex regexNumero = new Regex(@"^[0.0-9.9]+$");

        //cadena de expresiones regulares donde solo se aceptan letras
        Regex regexLetra = new Regex(@"^[a-zA-Z]+$");

        protected int cantPasajeros;//atributo
        public int CantPasajeros //propiedad
        {
            get { return this.cantPasajeros; }
            set { this.cantPasajeros = value; }
        }

        protected double capacidadGas;//atributo
        public double CapacidadGas //propiedad
        {
            get { return capacidadGas; }
            set { this.capacidadGas = value; }
        }

        //Constructor clase base
        public clsVehiculo()
        {
            this.cantPasajeros = 0;
            this.capacidadGas = 0;
        }

        public virtual double ConsumoGas(double capacidad)//Método virtual
        {
            /*
            virtualConsumoGas(): en base a la capacidad de gasolina de cada vehiculo y 1 Kilometro 
            se calculará el consumo de gas

            Equivalencias
            1 Km/Gal = 0.264172 km/litro
            1 Gal = 4.54609 Litros

            Operaciones

            Datos -> 
            C = Capacidad en galones de combustible del vehiculo
            paso los galones a litros

            1 Km / C Litros = 0.2199 km/litros

            */
            return 0;
        }

        public virtual string Registrar()//Método virtual
        {
            //virtualRegistrar(): Registra los datos de cada vehiculo en una lista
            return "";
        }


        //Método de validación sí es número
        public bool esNumero(string numero)
        {
            //Valida si no esta vacío          
            if (!String.IsNullOrEmpty(numero))
            {
                //Valida si es un número el dato
                if (regexNumero.IsMatch(numero))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        //Método de validación sí es letra
        public bool esPalabra(string palabra)
        {
            //Valida si no esta vacío          
            if (!String.IsNullOrEmpty(palabra))
            {
                //Valida si es un palabra el dato
                if (regexLetra.IsMatch(palabra))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
