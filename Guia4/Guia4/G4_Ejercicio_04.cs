﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guia4
{
    public partial class G4_Ejercicio_04 : Form
    {
        public G4_Ejercicio_04()
        {
            InitializeComponent();
            MenuControl.SelectedTab = tabPage3;
            llenaCombos();
        }

        private void llenaCombos()
        {
            //Llenado de comboBox para cantidad pasajeros - Motos
            cboCantPasajerosMoto.Items.Add("1");
            cboCantPasajerosMoto.Items.Add("2");
            cboCantPasajerosMoto.Items.Add("4");

            //Llenado de comboBox cantidad de llantas - Motos
            cboCantLlantasMoto.Items.Add("2");
            cboCantLlantasMoto.Items.Add("3");
            cboCantLlantasMoto.Items.Add("4");

            //Llenado de comboBox cantidad pasajeros - Carros
            cboCantPasajerosCarro.Items.Add("1");
            cboCantPasajerosCarro.Items.Add("2");
            cboCantPasajerosCarro.Items.Add("5");

            //Llenado de comboBox cantidad de puertas - Carros
            cboCantPuertasCarro.Items.Add("2");
            cboCantPuertasCarro.Items.Add("4");

            //Llenadoo de comboBox tipo de caja - Carros
            cboTipoCaja.Items.Add("Manual");
            cboTipoCaja.Items.Add("Automatico");

            //Llenado de comboBox pasajeris - Buses
            cboCantPasajerosBus.Items.Add("20");
            cboCantPasajerosBus.Items.Add("30");
            cboCantPasajerosBus.Items.Add("40");

            //Llenado de comboBox posición del motro - Buses
            cboPosicionMotor.Items.Add("Atrás");
            cboPosicionMotor.Items.Add("Delante");
        }

        private void limpiaCampos()
        {
            //Campos de MOTOS
            cboCantPasajerosMoto.Text = null;
            cboCantPasajerosMoto.Items.Clear();
            txtCapacidadGasMoto.Clear();
            txtColorMoto.Clear();
            cboCantLlantasMoto.Text = null;
            cboCantLlantasMoto.Items.Clear();

            //Campos de CARROS
            cboCantPasajerosCarro.Text = null;
            cboCantPasajerosCarro.Items.Clear();
            txtCapacidadGasCarro.Clear();
            cboCantPuertasCarro.Text = null;
            cboCantPuertasCarro.Items.Clear();
            cboTipoCaja.Text = null;
            cboTipoCaja.Items.Clear();

            //Campos de BUSES
            cboCantPasajerosBus.Text = null;
            cboCantPasajerosBus.Items.Clear();
            txtCapacidadGasBus.Clear();
            cboPosicionMotor.Text = null;
            cboPosicionMotor.Items.Clear();
            txtLongitudBus.Clear();

        }

        private void almacenaMotos()
        {
            //Guardo a través de las propiedades según su tipo de valor
            clsMoto moto = new clsMoto();
            moto.ObtieneDatos(cboCantPasajerosMoto.Text, txtCapacidadGasMoto.Text, txtColorMoto.Text, cboCantLlantasMoto.Text);

            //Limpio y lleno comboBoxes
            limpiaCampos();
            llenaCombos();

            //Obtengo cadena a imprimir y lo muevo al tabPage indicado
            listBox1.Items.Add(moto.Registrar());
            MenuControl.SelectedTab = tabPage5;

        }

        private void almacenaCarros()
        {
            //Guardo a través de las propiedades según su tipo de valor
            clsCarro carro = new clsCarro();
            carro.ObtieneDatos(cboCantPasajerosCarro.Text, txtCapacidadGasCarro.Text, cboCantPuertasCarro.Text, cboTipoCaja.Text);

            //Limpio y lleno comboBoxes
            limpiaCampos();
            llenaCombos();

            //Obtengo cadena a imprimir de los resultados calculados por los métodos
            listBox1.Items.Add(carro.Registrar());
            MenuControl.SelectedTab = tabPage5;
        }

        private void almacenaBuses()
        {

            //Con la creación del objeto guardo a través de las propiedades según su tipo de valor
            clsBus bus = new clsBus();
            bus.ObtieneDatos(cboCantPasajerosBus.Text, txtCapacidadGasBus.Text, cboPosicionMotor.Text, txtLongitudBus.Text);

            //Limpio y lleno comboBoxes
            limpiaCampos();
            llenaCombos();

            //Obtengo cadena a imprimir de los resultados calculados por los métodos
            listBox1.Items.Add(bus.Registrar());
            MenuControl.SelectedTab = tabPage5;
        }

        private void btnRegistroMotos_Click(object sender, EventArgs e)
        {
            //Con el botón regidirgo al tabPage que corresponde al registro del vehiculo
            MenuControl.SelectedTab = tabPage1;
        }

        private void btnRegistroCarros_Click(object sender, EventArgs e)
        {
            //Con el botón regidirgo al tabPage que corresponde al registro del vehiculo
            MenuControl.SelectedTab = tabPage2;
        }

        private void btnRegistroBuses_Click(object sender, EventArgs e)
        {
            //Con el botón regidirgo al tabPage que corresponde al registro del vehiculo
            MenuControl.SelectedTab = tabPage4;
        }

        private void btnVerRegistro_Click(object sender, EventArgs e)
        {
            //Con el botón regidirgo al tabPage que corresponde al registro del vehiculo
            MenuControl.SelectedTab = tabPage5;
        }

        private void btnRegistraMoto_Click(object sender, EventArgs e)
        {
            //Llamo función que valida y guarda mis datos
            almacenaMotos();
        }

        private void btnRegistraCarro_Click(object sender, EventArgs e)
        {
            //Llamo función que valida y guarda mis datos
            almacenaCarros();
        }

        private void btnRegistraBus_Click(object sender, EventArgs e)
        {
            //Llamo función que valida y guarda mis datos
            almacenaBuses();
        }

        private void btnRegresar_Click(object sender, EventArgs e)
        {
            //Con el botón regidirgo al tabPage que corresponde al registro del vehiculo
            MenuControl.SelectedTab = tabPage3;
            limpiaCampos();
            llenaCombos();
        }
    }
}
