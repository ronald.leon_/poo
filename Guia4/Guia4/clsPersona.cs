﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Guia4
{
    public class clsPersona
    {
        //Patron de validación para números con expresión regular
        Regex regexNumero = new Regex(@"^[0.0-9.9]+$");

        //cadena de expresiones regulares donde solo se aceptan letras
        Regex regexLetra = new Regex(@"^[a-zA-Z]+$");


        protected string nombre;//atributo
        public string Nombre //propiedad
        {
            get { return nombre; }
            set { this.nombre = value; }
        }

        //Constructor clase base
        public clsPersona()
        {
            this.nombre = "";
        }

        //Método que obtiene los datos a registrar y los valida
        public string ObtieneDatos(string nombre)
        {
            //Remplazo los caracteres y espacios para validar string porque la expresión regular no contempla espacio
            if (esPalabra(nombre.Replace(" ", "")))
                this.Nombre = nombre;

            return "";
        }

        //Método de validación sí es número
        public bool esNumero(string numero)
        {
            //Valida si no esta vacío          
            if (!String.IsNullOrEmpty(numero))
            {
                //Valida si es un número el dato
                if (regexNumero.IsMatch(numero))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        //Método de validación sí es letra
        public bool esPalabra(string palabra)
        {
            //Valida si no esta vacío          
            if (!String.IsNullOrEmpty(palabra))
            {
                //Valida si es un palabra el dato
                if (regexLetra.IsMatch(palabra))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
