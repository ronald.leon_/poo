﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Guia4
{
    public partial class FrmEstudiante : Guia4.FrmRegistro
    {
        public FrmEstudiante()
        {
            InitializeComponent();
        }

        /*listado que permite tener varios elementos de la clase Individuo*/
        private List<Individuo> Estudiantes = new List<Individuo>();
        //índice para editar comienza en -1, significa que no hay ninguno seleccionado, esto servirá para el DataGridView.
        private int edit_indice = -1;

        private void FrmEstudiante_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void limpiar()
        {
            txtNombre.Clear();
            txtUsuario.Clear();
            txtCodigo.Clear();
        }

        private void actualizarGrid()
        {
            dataGridView1.DataSource = null;
            /*los nombres de columna que veremos son los de las propiedades*/
            dataGridView1.DataSource = Estudiantes;
        }

        private void almacena()
        {
            if (!string.IsNullOrEmpty(txtNombre.Text) && !string.IsNullOrEmpty(txtUsuario.Text) && !string.IsNullOrEmpty(txtCodigo.Text))
            {
                //cadena de expresiones regulares donde solo se aceptan letras
                Regex regexLetra = new Regex(@"^[a-zA-Z]+$");

                if (regexLetra.IsMatch(txtNombre.Text))
                {
                    //Creo un objeto de la clase y guardo a través de las propiedades
                    Individuo estudiante = new Individuo();
                    estudiante.Nombre = txtNombre.Text;
                    estudiante.Usuario = txtUsuario.Text;
                    estudiante.Codigo = txtCodigo.Text;

                    if (edit_indice > -1) //verifica si hay un indice seleccionado
                    {
                        Estudiantes[edit_indice] = estudiante;
                        edit_indice = -1;
                    }
                    else
                    {
                        //Al arreglo de productos le agrego el objeto creado con todos los datos que recolecté
                        Estudiantes.Add(estudiante);
                    }

                    //Limpio los campos para futuros registros y actualizo el grid
                    actualizarGrid();
                    limpiar();
                }
                else
                {
                    MessageBox.Show("¡Solo se aceptan letras para el nombre!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("¡Rellene el campo vacío!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void edita()
        {
            DataGridViewRow selected = dataGridView1.SelectedRows[0];
            int posicion = dataGridView1.Rows.IndexOf(selected);//almacena en cual fila estoy
            edit_indice = posicion; //copia esa variable en indice editado

            //Esta variable de tipo persona, se carga con los valores que le pasa el listado
            Individuo estudiante = Estudiantes[posicion];

            //lo que tiene el atributo se lo doy al textbox
            txtNombre.Text = estudiante.Nombre;
            txtUsuario.Text = estudiante.Usuario;
            txtCodigo.Text = estudiante.Codigo;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            //Función que guarda/actualiza los datos
            almacena();
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            //Función que limpiar los campos para futuros registros
            limpiar();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            //Función que selecciona los datos a editar
            edita();
        }
    }
}
