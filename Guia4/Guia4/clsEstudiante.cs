﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guia4
{
    public class clsEstudiante : clsPersona
    {
        private string carnet;//atributo
        public string Carnet //propiedad
        {
            get { return carnet; }
            set { this.carnet = value; }
        }

        private string nivelEstudio;//atributo
        public string NivelEstudio //propiedad
        {
            get { return nivelEstudio; }
            set { this.nivelEstudio = value; }
        }

        //Constructor
        public clsEstudiante()
        {
            this.carnet = "";
            this.nivelEstudio = "";
        }
    }
}
