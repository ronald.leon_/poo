﻿using G2_Ejercicio_01;
using G2_Ejercicio_02;
using G2_Ejercicio_03;
using G2_Ejercicio_04;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Menu
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtNeto Ejercicio1 = new txtNeto();
            Ejercicio1.Show();
        }

        private void btnDos_Click(object sender, EventArgs e)
        {
            frmConversiones Ejercicio2 = new frmConversiones();
            Ejercicio2.Show();
        }

        private void btnTres_Click(object sender, EventArgs e)
        {
            frmCuadratica Ejercicio3 = new frmCuadratica();
            Ejercicio3.Show();
        }

        private void btnCuatro_Click(object sender, EventArgs e)
        {
            frmCalculosArreglos Ejercicio4 = new frmCalculosArreglos();
            Ejercicio4.Show();
        }
    }
}
