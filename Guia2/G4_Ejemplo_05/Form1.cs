﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace G4_Ejemplo_05
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            listArreglo.Items.Add(txtNumero.Text);
            txtNumero.Clear();
            txtNumero.Focus();
        }

        //Evento calcular al mayor de los pares negativos
        private void btnCalc1_Click(object sender, EventArgs e)
        {
<<<<<<< HEAD
            int mayorneg = 1000;
=======
            int mayorneg = -1000;
>>>>>>> revisar
            for(int i = 0; i < listArreglo.Items.Count; i++)
            {
                string valor = listArreglo.Items[i].ToString();
                int numero = int.Parse(valor);

                if(numero < 0 && numero % 2 == 0)
                {
                    if(numero > mayorneg)
                    {
                        mayorneg = numero;
                        txtCalc1.Text = mayorneg.ToString();
                    }
                }
                else
                {
                    txtCalc1.Text = "No hay números negativos pares";
                }
            }
        }

        //Evento para calcular porcentaje de ceros
        private void btnCalc2_Click(object sender, EventArgs e)
        {
            double cantidadnumeros = listArreglo.Items.Count;

            double cantidadceros = 0;
            double porcentaje = 0;

            for (int i = 0; i < listArreglo.Items.Count; i++)
            {
                string valor = listArreglo.Items[i].ToString();
                int numero = int.Parse(valor);

                if(numero == 0)
                {
                    cantidadceros = cantidadceros + 1;
                }
            }
            porcentaje = (cantidadceros / cantidadnumeros) * 100;
            txtCalc2.Text = porcentaje.ToString() + "%";
        }

        //Evento para obtener el promedio de impares positivos
        private void btnCalc3_Click(object sender, EventArgs e)
        {
            double prom;
            double cantidadimpares = 0;
            double suma = 0;

            for(int i = 0; i < listArreglo.Items.Count; i++)
            {
                string valor = listArreglo.Items[i].ToString();
                int numero = int.Parse(valor);

                if(numero > 0 && numero % 2 != 0)
                {
                    suma = suma + numero;
                    cantidadimpares = cantidadimpares + 1;
                }
            }
            prom = suma / cantidadimpares;
            txtCalc3.Text = prom.ToString();
        }

        private void btnCalc4_Click(object sender, EventArgs e)
        {
            int mayor = 0;

            for(int i = 0; i < listArreglo.Items.Count; i++)
            {
                string valor = listArreglo.Items[i].ToString();
                int numero = int.Parse(valor);

                if(numero > 0 && numero % 2 == 0)
                {
                    if(numero > mayor)
                    {
                        mayor = numero;
                    }
                }
            }
            txtCalc4.Text = mayor.ToString();
        }
    }
}
