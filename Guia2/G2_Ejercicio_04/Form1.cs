﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace G2_Ejercicio_04
{
    public partial class frmCalculosArreglos : Form
    {
        public frmCalculosArreglos()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            //NÚMERO MAYOR DE PARES NEGATIVOS:
            if (listArreglo.Items.Count != 0)
            {
                int mayorneg = -1000;
                for (int i = 0; i < listArreglo.Items.Count; i++)
                {
                    string valor = listArreglo.Items[i].ToString();
                    int numero = int.Parse(valor);
                    if (numero < 0 && numero % 2 == 0)
                    {
                        if (numero > mayorneg)
                        {
                            mayorneg = numero;
                            txtCalc1.Text = mayorneg.ToString();
                        }
                    }
                    else
                    {
                        txtCalc1.Text = "No hay números negativos pares";
                    }
                }
            


                //PORCENTAJE DE CEROS EN EL ARREGLO
                double cantidadnumeros = listArreglo.Items.Count;
                double cantidadceros = 0;
                double porcentaje = 0;
                for (int i = 0; i < listArreglo.Items.Count; i++)
                {
                    string valor = listArreglo.Items[i].ToString();
                    int numero = int.Parse(valor);
                    if (numero == 0)
                    {
                        cantidadceros = cantidadceros + 1;
                    }
                }
                porcentaje = (cantidadceros / cantidadnumeros) * 100;
                txtCalc2.Text = porcentaje.ToString() + "%";




                //PROMEDIO DE IMPARES POSITIVOS
                double prom;
                double cantidadimpares = 0;
                double suma = 0;
                for (int i = 0; i < listArreglo.Items.Count; i++)
                {
                    string valor = listArreglo.Items[i].ToString();
                    int numero = int.Parse(valor);
                    if (numero > 0 && numero % 2 != 0)
                    {
                        suma = suma + numero;

                        cantidadimpares = cantidadimpares + 1;

                    }
                }
                prom = suma / cantidadimpares;
                txtCalc3.Text = prom.ToString();



                //MAYOR DE LOS PARES POSITIVOS
                int mayor = 0;
                for (int i = 0; i < listArreglo.Items.Count; i++)
                {
                    string valor = listArreglo.Items[i].ToString();
                    int numero = int.Parse(valor);
                    if (numero > 0 && numero % 2 == 0)
                    {
                        if (numero > mayor)
                            mayor = numero;
                    }
                }
                txtCalc4.Text = mayor.ToString();
            }
            else
            {
                lblAviso.Text = "¡No hay datos que procesar!";
            }
        }

        //Evento del textbox para que al presionar la tecla enter este agrega los datos al listbox
        private void txtNumero_KeyPress(object sender, KeyPressEventArgs e)
        {

            if(txtNumero.Text != "")
            {
                //cadena de expresiones regulares donde solo se aceptan números(positivos y negativos)
                Regex regex = new Regex(@"[0-9]+$");


                //validación si el campo de txtNumero contiene solo número comparando el campo con la expresión regular
                if (regex.IsMatch(txtNumero.Text))
                {
                    //al presionar el enter añade lo del textbox al listbox
                    if (e.KeyChar == (int)Keys.Enter)
                    {
                        listArreglo.Items.Add(txtNumero.Text);
                        txtNumero.Clear();
                        txtNumero.Focus();
                    }
                    else
                    {
                        lblAviso.Text = "¡Presiona enter para ingresar los datos!";
                    }
                }
                else
                {
                    lblAviso.Text = "¡Solo permiten números!";
                }
            }
            else
            {
                lblAviso.Text = "¡Digita algo en el campo vacío!";
            }



            
        }

        private void txtNumero_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
