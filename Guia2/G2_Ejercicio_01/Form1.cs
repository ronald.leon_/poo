﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace G2_Ejercicio_01
{
    public partial class txtNeto : Form
    {
        public txtNeto()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        
        private void btnCalcular_Click(object sender, EventArgs e)
        {
            //declaración de variables
            double salario, descuento, neto, saldoMenos;
            string bruto, nombre, apellido;
            bool esNumero;
            int num;

            //capturación de algunos datos
            nombre = txtNombre.Text;
            apellido = txtApellido.Text;
            bruto = txtBruto.Text;
            

            //validación de campos vacíos
            if (bruto != "" && nombre != "" && apellido != "" )
            {
                //cadena de expresiones regulares donde solo se aceptan números
                Regex regex = new Regex(@"^[0-9]+$");

                //validación si el campo de txtBruto contiene solo número comparando el campo con la expresión regular
                if (regex.IsMatch(bruto))
                {
                    //conversión de string a double del salario
                    salario = double.Parse(bruto);

                    
                    if (rbtnGerente.Checked == true)
                    {
                        txtDesc.Text = "20%";
                        lblRespuesta.ResetText();

                        //capturo el descuento en dolares y lo muestro en el txt
                        saldoMenos = salario * 0.20;
                        txtSaldoDesc.Text = saldoMenos.ToString();

                        //calculo el monto final con el descuentos y lo muestro en el txt
                        neto = salario - saldoMenos;
                        txtSaldoNeto.Text = neto.ToString();

                        //cambio de texto del label para mostar el nombre del empleado más su tipo
                        lblRespuesta.Text = "Este sería su descuento, Gerente " + nombre + " " + apellido;
                    }
                    else if (rbtnSubGe.Checked == true)
                    {
                        txtDesc.Text = "15%";
                        lblRespuesta.ResetText();

                        //capturo el descuento en dolares y lo muestro en el txt
                        saldoMenos = salario * 0.15;
                        txtSaldoDesc.Text = saldoMenos.ToString();

                        //calculo el monto final con el descuentos y lo muestro en el txt
                        neto = salario - saldoMenos;
                        txtSaldoNeto.Text = neto.ToString();

                        //cambio de texto del label para mostar el nombre del empleado más su tipo
                        lblRespuesta.Text = "Este sería su descuento, SubGerente " + nombre + " " + apellido;
                    }
                    else if (rbtnSecre.Checked == true)
                    {
                        txtDesc.Text = "5%";
                        lblRespuesta.ResetText();

                        //capturo el descuento en dolares y lo muestro en el txt
                        saldoMenos = salario * 0.05;
                        txtSaldoDesc.Text = saldoMenos.ToString();

                        //calculo el monto final con el descuentos y lo muestro en el txt
                        neto = salario - saldoMenos;
                        txtSaldoNeto.Text = neto.ToString();

                        //cambio de texto del label para mostar el nombre del empleado más su tipo
                        lblRespuesta.Text = "Este sería su descuento, Secretaria " + nombre + " " + apellido;
                    }
                    else
                    {
                        lblRespuesta.Text = "¡Selccione que tipo de empleado eres!";
                    }
                }
                else
                {
                    lblRespuesta.Text = "¡Tu salario debe ser en números!";
                }
            }
            else
            {
                lblRespuesta.Text = "¡Rellenos los campos faltantes!";
            }            
        }
                
       
        private void rbtnGerente_CheckedChanged(object sender, EventArgs e)
        {
            if(rbtnGerente.Checked == true)
            {
                txtDesc.Text = "20%";
                txtSaldoDesc.Clear();
                txtSaldoNeto.Clear();
                lblRespuesta.ResetText();
            }
        }

        private void txtBruto_TextChanged(object sender, EventArgs e)
        {
            
        }


        private void rbtnSubGe_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtnSubGe.Checked == true)
            {
                txtDesc.Text = "15%";
                txtSaldoDesc.Clear();
                txtSaldoNeto.Clear();
                lblRespuesta.ResetText();
            }
        }

        private void rbtnSecre_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtnSecre.Checked == true)
            {
                txtDesc.Text = "5%";
                txtSaldoDesc.Clear();
                txtSaldoNeto.Clear();
                lblRespuesta.ResetText();
            }
        }
    }
}
