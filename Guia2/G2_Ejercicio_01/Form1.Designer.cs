﻿
namespace G2_Ejercicio_01
{
    partial class txtNeto
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtApellido = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtBruto = new System.Windows.Forms.TextBox();
            this.txtDesc = new System.Windows.Forms.TextBox();
            this.txtSaldoDesc = new System.Windows.Forms.TextBox();
            this.rbtnGerente = new System.Windows.Forms.RadioButton();
            this.rbtnSubGe = new System.Windows.Forms.RadioButton();
            this.rbtnSecre = new System.Windows.Forms.RadioButton();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.lblRespuesta = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtSaldoNeto = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(231, 28);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "DESCUENTOS";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 89);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nombre:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(297, 87);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Apellidos:";
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(101, 85);
            this.txtNombre.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(165, 22);
            this.txtNombre.TabIndex = 3;
            // 
            // txtApellido
            // 
            this.txtApellido.Location = new System.Drawing.Point(371, 85);
            this.txtApellido.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtApellido.Name = "txtApellido";
            this.txtApellido.Size = new System.Drawing.Size(165, 22);
            this.txtApellido.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(40, 176);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "Salario bruto:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(41, 239);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(150, 17);
            this.label5.TabIndex = 6;
            this.label5.Text = "Porcentaje descuento:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(48, 300);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(126, 17);
            this.label6.TabIndex = 7;
            this.label6.Text = "Saldo descontado:";
            // 
            // txtBruto
            // 
            this.txtBruto.Location = new System.Drawing.Point(201, 176);
            this.txtBruto.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtBruto.Name = "txtBruto";
            this.txtBruto.Size = new System.Drawing.Size(132, 22);
            this.txtBruto.TabIndex = 8;
            this.txtBruto.TextChanged += new System.EventHandler(this.txtBruto_TextChanged);
            // 
            // txtDesc
            // 
            this.txtDesc.Location = new System.Drawing.Point(201, 235);
            this.txtDesc.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDesc.Name = "txtDesc";
            this.txtDesc.ReadOnly = true;
            this.txtDesc.Size = new System.Drawing.Size(132, 22);
            this.txtDesc.TabIndex = 9;
            // 
            // txtSaldoDesc
            // 
            this.txtSaldoDesc.Location = new System.Drawing.Point(201, 300);
            this.txtSaldoDesc.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSaldoDesc.Name = "txtSaldoDesc";
            this.txtSaldoDesc.ReadOnly = true;
            this.txtSaldoDesc.Size = new System.Drawing.Size(132, 22);
            this.txtSaldoDesc.TabIndex = 10;
            // 
            // rbtnGerente
            // 
            this.rbtnGerente.AutoSize = true;
            this.rbtnGerente.Location = new System.Drawing.Point(471, 218);
            this.rbtnGerente.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbtnGerente.Name = "rbtnGerente";
            this.rbtnGerente.Size = new System.Drawing.Size(81, 21);
            this.rbtnGerente.TabIndex = 11;
            this.rbtnGerente.TabStop = true;
            this.rbtnGerente.Text = "Gerente";
            this.rbtnGerente.UseVisualStyleBackColor = true;
            this.rbtnGerente.CheckedChanged += new System.EventHandler(this.rbtnGerente_CheckedChanged);
            // 
            // rbtnSubGe
            // 
            this.rbtnSubGe.AutoSize = true;
            this.rbtnSubGe.Location = new System.Drawing.Point(471, 277);
            this.rbtnSubGe.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbtnSubGe.Name = "rbtnSubGe";
            this.rbtnSubGe.Size = new System.Drawing.Size(103, 21);
            this.rbtnSubGe.TabIndex = 12;
            this.rbtnSubGe.TabStop = true;
            this.rbtnSubGe.Text = "Subgerente";
            this.rbtnSubGe.UseVisualStyleBackColor = true;
            this.rbtnSubGe.CheckedChanged += new System.EventHandler(this.rbtnSubGe_CheckedChanged);
            // 
            // rbtnSecre
            // 
            this.rbtnSecre.AutoSize = true;
            this.rbtnSecre.Location = new System.Drawing.Point(471, 335);
            this.rbtnSecre.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbtnSecre.Name = "rbtnSecre";
            this.rbtnSecre.Size = new System.Drawing.Size(94, 21);
            this.rbtnSecre.TabIndex = 13;
            this.rbtnSecre.TabStop = true;
            this.rbtnSecre.Text = "Secretaria";
            this.rbtnSecre.UseVisualStyleBackColor = true;
            this.rbtnSecre.CheckedChanged += new System.EventHandler(this.rbtnSecre_CheckedChanged);
            // 
            // btnCalcular
            // 
            this.btnCalcular.Location = new System.Drawing.Point(213, 412);
            this.btnCalcular.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(100, 28);
            this.btnCalcular.TabIndex = 14;
            this.btnCalcular.Text = "Calcular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // lblRespuesta
            // 
            this.lblRespuesta.AutoSize = true;
            this.lblRespuesta.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRespuesta.ForeColor = System.Drawing.Color.Red;
            this.lblRespuesta.Location = new System.Drawing.Point(29, 469);
            this.lblRespuesta.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRespuesta.Name = "lblRespuesta";
            this.lblRespuesta.Size = new System.Drawing.Size(40, 25);
            this.lblRespuesta.TabIndex = 15;
            this.lblRespuesta.Text = "R//";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(51, 367);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 17);
            this.label7.TabIndex = 16;
            this.label7.Text = "Saldo Neto:";
            // 
            // txtSaldoNeto
            // 
            this.txtSaldoNeto.Location = new System.Drawing.Point(201, 358);
            this.txtSaldoNeto.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSaldoNeto.Name = "txtSaldoNeto";
            this.txtSaldoNeto.ReadOnly = true;
            this.txtSaldoNeto.Size = new System.Drawing.Size(132, 22);
            this.txtSaldoNeto.TabIndex = 17;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(467, 176);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(122, 17);
            this.label8.TabIndex = 18;
            this.label8.Text = "Tipo de empleado";
            // 
            // txtNeto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(639, 527);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtSaldoNeto);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblRespuesta);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.rbtnSecre);
            this.Controls.Add(this.rbtnSubGe);
            this.Controls.Add(this.rbtnGerente);
            this.Controls.Add(this.txtSaldoDesc);
            this.Controls.Add(this.txtDesc);
            this.Controls.Add(this.txtBruto);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtApellido);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "txtNeto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ventana de empleados";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtApellido;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtBruto;
        private System.Windows.Forms.TextBox txtDesc;
        private System.Windows.Forms.TextBox txtSaldoDesc;
        private System.Windows.Forms.RadioButton rbtnGerente;
        private System.Windows.Forms.RadioButton rbtnSubGe;
        private System.Windows.Forms.RadioButton rbtnSecre;
        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.Label lblRespuesta;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtSaldoNeto;
        private System.Windows.Forms.Label label8;
    }
}

