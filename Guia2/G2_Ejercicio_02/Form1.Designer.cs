﻿
namespace G2_Ejercicio_02
{
    partial class frmConversiones
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.rbtnTemp = new System.Windows.Forms.RadioButton();
            this.rbtnLong = new System.Windows.Forms.RadioButton();
            this.rbtnPeso = new System.Windows.Forms.RadioButton();
            this.txtUno = new System.Windows.Forms.TextBox();
            this.txtDos = new System.Windows.Forms.TextBox();
            this.lblUno = new System.Windows.Forms.Label();
            this.lblDos = new System.Windows.Forms.Label();
            this.brnCalcular = new System.Windows.Forms.Button();
            this.lblAviso = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(145, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "CONVERSIONES";
            // 
            // rbtnTemp
            // 
            this.rbtnTemp.AutoSize = true;
            this.rbtnTemp.Location = new System.Drawing.Point(22, 89);
            this.rbtnTemp.Name = "rbtnTemp";
            this.rbtnTemp.Size = new System.Drawing.Size(111, 21);
            this.rbtnTemp.TabIndex = 1;
            this.rbtnTemp.TabStop = true;
            this.rbtnTemp.Text = "Temperatura";
            this.rbtnTemp.UseVisualStyleBackColor = true;
            this.rbtnTemp.CheckedChanged += new System.EventHandler(this.rbtnTemp_CheckedChanged);
            // 
            // rbtnLong
            // 
            this.rbtnLong.AutoSize = true;
            this.rbtnLong.Location = new System.Drawing.Point(178, 89);
            this.rbtnLong.Name = "rbtnLong";
            this.rbtnLong.Size = new System.Drawing.Size(84, 21);
            this.rbtnLong.TabIndex = 2;
            this.rbtnLong.TabStop = true;
            this.rbtnLong.Text = "Longitud";
            this.rbtnLong.UseVisualStyleBackColor = true;
            this.rbtnLong.CheckedChanged += new System.EventHandler(this.rbtnLong_CheckedChanged);
            // 
            // rbtnPeso
            // 
            this.rbtnPeso.AutoSize = true;
            this.rbtnPeso.Location = new System.Drawing.Point(314, 89);
            this.rbtnPeso.Name = "rbtnPeso";
            this.rbtnPeso.Size = new System.Drawing.Size(61, 21);
            this.rbtnPeso.TabIndex = 3;
            this.rbtnPeso.TabStop = true;
            this.rbtnPeso.Text = "Peso";
            this.rbtnPeso.UseVisualStyleBackColor = true;
            this.rbtnPeso.CheckedChanged += new System.EventHandler(this.rbtnPeso_CheckedChanged);
            // 
            // txtUno
            // 
            this.txtUno.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUno.Location = new System.Drawing.Point(12, 155);
            this.txtUno.Name = "txtUno";
            this.txtUno.Size = new System.Drawing.Size(100, 27);
            this.txtUno.TabIndex = 4;
            this.txtUno.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtDos
            // 
            this.txtDos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDos.Location = new System.Drawing.Point(229, 155);
            this.txtDos.Name = "txtDos";
            this.txtDos.ReadOnly = true;
            this.txtDos.Size = new System.Drawing.Size(100, 27);
            this.txtDos.TabIndex = 5;
            this.txtDos.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblUno
            // 
            this.lblUno.AutoSize = true;
            this.lblUno.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUno.Location = new System.Drawing.Point(118, 155);
            this.lblUno.Name = "lblUno";
            this.lblUno.Size = new System.Drawing.Size(15, 20);
            this.lblUno.TabIndex = 6;
            this.lblUno.Text = "-";
            // 
            // lblDos
            // 
            this.lblDos.AutoSize = true;
            this.lblDos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDos.Location = new System.Drawing.Point(335, 155);
            this.lblDos.Name = "lblDos";
            this.lblDos.Size = new System.Drawing.Size(15, 20);
            this.lblDos.TabIndex = 7;
            this.lblDos.Text = "-";
            // 
            // brnCalcular
            // 
            this.brnCalcular.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.brnCalcular.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.brnCalcular.Location = new System.Drawing.Point(148, 208);
            this.brnCalcular.Name = "brnCalcular";
            this.brnCalcular.Size = new System.Drawing.Size(89, 29);
            this.brnCalcular.TabIndex = 8;
            this.brnCalcular.Text = "Calcular";
            this.brnCalcular.UseVisualStyleBackColor = true;
            this.brnCalcular.Click += new System.EventHandler(this.brnCalcular_Click);
            // 
            // lblAviso
            // 
            this.lblAviso.AutoSize = true;
            this.lblAviso.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAviso.ForeColor = System.Drawing.Color.Red;
            this.lblAviso.Location = new System.Drawing.Point(45, 250);
            this.lblAviso.Name = "lblAviso";
            this.lblAviso.Size = new System.Drawing.Size(20, 25);
            this.lblAviso.TabIndex = 9;
            this.lblAviso.Text = "-";
            // 
            // frmConversiones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(453, 294);
            this.Controls.Add(this.lblAviso);
            this.Controls.Add(this.brnCalcular);
            this.Controls.Add(this.lblDos);
            this.Controls.Add(this.lblUno);
            this.Controls.Add(this.txtDos);
            this.Controls.Add(this.txtUno);
            this.Controls.Add(this.rbtnPeso);
            this.Controls.Add(this.rbtnLong);
            this.Controls.Add(this.rbtnTemp);
            this.Controls.Add(this.label1);
            this.Name = "frmConversiones";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Conversiones";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbtnTemp;
        private System.Windows.Forms.RadioButton rbtnLong;
        private System.Windows.Forms.RadioButton rbtnPeso;
        private System.Windows.Forms.TextBox txtUno;
        private System.Windows.Forms.TextBox txtDos;
        private System.Windows.Forms.Label lblUno;
        private System.Windows.Forms.Label lblDos;
        private System.Windows.Forms.Button brnCalcular;
        private System.Windows.Forms.Label lblAviso;
    }
}

