﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicios
{
    class Alumno
    {
        
        
        string carnet;
        public string Carnet
        {
            get { return carnet; }
            set { carnet = value; }
        }


        string nombre;
        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }


        string apellido;
        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }


        string materia;
        public string Materia
        {
            get { return materia; }
            set { materia = value; }
        }


        string[] calificaciones = new string[3];
        public string[] Calificaciones
        {
            get { return calificaciones; }
            set { calificaciones = value; }
        }


        //Métodos
        
        public void IngresaEstudiantes(string Carnet, string Nombre, string Apellido, string Materia, string Nota1, string Nota2, string Nota3)
        {
            carnet = Carnet;
            nombre = Nombre;
            apellido = Apellido;
            materia = Materia;

            int i;
            for (i = 0; i < 3; i++)
            {

                calificaciones[i] = Nota1 + " " + Nota2 + " " + Nota3;
            }
        }
    }
}
