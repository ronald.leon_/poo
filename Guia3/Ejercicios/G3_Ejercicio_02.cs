﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicios
{
    public partial class G3_Ejercicio_02 : Form
    {
        Alumno[] Estudiante;
        
        public G3_Ejercicio_02()
        {
            InitializeComponent();
            gboxMenu.Enabled = false;
            gboxDatosEstudiantes.Visible = false;
            listBoxListado.Visible = false;
            //listBoxReporte.Visible = false;
        }

        
        private void btnNumEstudiante_Click(object sender, EventArgs e)
        {
            if(txtNumEstudiantes.Text != " " || txtNumEstudiantes.Text != null)
            {
                //cadena de expresiones regulares donde solo se aceptan números positivos
                Regex regexNumero = new Regex(@"^[0-9]+$");
                //Si lo escrito en el campo son números sigue...
                if (regexNumero.IsMatch(txtNumEstudiantes.Text))
                {
                    //Captura la cantidad de estudiantes que sera mi cantidad de objetos
                    int cantEstudiantes = int.Parse(txtNumEstudiantes.Text);
                    if (cantEstudiantes != 0)
                    {
                        Estudiante = new Alumno[cantEstudiantes];
                        txtNumEstudiantes.Enabled = false;
                        gboxMenu.Enabled = true;
                        btnVerEstudiantes.Enabled = false;
                        btnReporte.Enabled = false;
                    }
                    else
                    {
                        MessageBox.Show("¡Solo se aceptan números diferentes de cero!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("¡Solo se aceptan números!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else
            {
                MessageBox.Show("¡Relleno el campo vacío!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


            
                   
        }

        public void limpiar()
        {
            txtCarnet.Clear();
            txtNombre.Clear();
            txtApellido.Clear();
            txtMateria.Clear();
            txtNota1.Clear();
            txtNota2.Clear();
            txtNota3.Clear();
        }


        int contador = 0;
        public void btnDatosEstudiantes_Click(object sender, EventArgs e)
        {

            if (txtNombre.Text != " " && txtApellido.Text != " " && txtMateria.Text != " " && txtNota1.Text != " " && txtNota2.Text != " " && txtNota3.Text != " ")
            {
                //cadena de expresiones regulares donde solo se aceptan letras
                Regex regexLetra = new Regex(@"^[a-zA-Z]+$");

                //Si lo escrito en el campo son letras sigue...
                if (regexLetra.IsMatch(txtNombre.Text) && regexLetra.IsMatch(txtApellido.Text)  && regexLetra.IsMatch(txtMateria.Text))
                {
                    //cadena de expresiones regulares donde solo se aceptan números positivos
                    Regex regexNumero = new Regex(@"^[0-9]+$");

                    //Si lo escrito en el campo son números sigue...
                    if (regexNumero.IsMatch(txtNota1.Text) && regexNumero.IsMatch(txtNota2.Text) && regexNumero.IsMatch(txtNota3.Text))
                    {
                        //si las notas no superan el rango de 10 sigue...
                        if (int.Parse(txtNota1.Text) <= 10 && int.Parse(txtNota2.Text) <= 10 && int.Parse(txtNota3.Text) <= 10)
                        {
                            //Sí el contador es menor a la cantidad de estudiantes que se definio al inicio sigue...
                            if (contador < Estudiante.Length)
                            {
                                //Creo el objeto apartir del estudiante que se vaya creando
                                Estudiante[contador] = new Alumno();
                                //Mando los parametros al metodo en la clase
                                Estudiante[contador].IngresaEstudiantes(txtCarnet.Text, txtNombre.Text, txtApellido.Text, txtMateria.Text, txtNota1.Text, txtNota2.Text, txtNota3.Text);

                                //Por cada nuevo estudiante incremento mi contador
                                contador++;

                                //Limpio los campos para ingresar el siguiente estudiante
                                limpiar();

                                //Mensaje para que ingrese el estudiante sigueinte
                                if (contador < Estudiante.Length)
                                {
                                    MessageBox.Show("Ingrese el siguiente estudiante", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                else
                                {
                                    //En caso de que se llegue al maxímo de estudaintes muestro lo suguiente y habilito los dempas botones
                                    MessageBox.Show("Datos completamente ingresados!", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                                    btnVerEstudiantes.Enabled = true;
                                    btnReporte.Enabled = true;
                                }

                            }
                            //Sí el contador que indica la cantidad de estudiantes ingresados es igualada a la cantidad indicada se bloquea los campos 
                            if (contador == Estudiante.Length)
                            {
                                gboxDatosEstudiantes.Enabled = false;
                            }
                        }
                        else
                        {
                            MessageBox.Show("¡El rango de notas es entre 0 y 10!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("¡Solo se aceptan números para las notas!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("¡Solo se aceptan letras para el nombre, apellido o materia!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("¡Relleno los campos vacios!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
        }

        private void btnNuevoEstudiante_Click(object sender, EventArgs e)
        {
            //Habilito el formulario para ingresar los datos de los estudiantes
            gboxDatosEstudiantes.Visible = true;
        }

        private void btnReporte_Click(object sender, EventArgs e)
        {
            //Limpio el listBox en caso se vuelva a dar click en el boton para no repetir lo mismo
            listBoxReporte.Items.Clear();
           
            //Pongo visible la lista de reportes
            listBoxReporte.Visible = true;

            //Con un for recorro cada uno de los campos y muestro cada estudainte con su respectiva información
            for (int i = 0; i < Estudiante.Length; i++)
            {
                //Obtengo las notas almacenadas como STRING y separadas por espacio para mostrarlas separadas por guiones
                string muestraNotas = Estudiante[i].Calificaciones[i].Replace(" ", ", ");

                //Obtengo las notas originales separadas por espacio y lo descompongo en un array separado por espacios
                string[] numeros = Estudiante[i].Calificaciones[i].ToString().Split(new char[] { ' ' });

                //recorro el array sumando sus elementos para sacar su promedio
                float suma = 0;
                foreach (string numero in numeros)
                {
                    suma += Convert.ToInt64(numero);
                }
                //Calculo promedio de notas
                float promedio = suma / 3;

                //Creo sintaxis del reporte final de los estudiantes agregando todo al listBox
                listBoxReporte.Items.Add("El alumno " + Estudiante[i].Nombre + " " + Estudiante[i].Apellido + " con carnet " + Estudiante[i].Carnet + " está cursando la materia " +
                                      Estudiante[i].Materia + " y sus notas en esta asignatura son:  " + muestraNotas + "  y su promedio es de:  " + Math.Round(promedio,2));
            }
        }

        private void btnVerEstudiantes_Click(object sender, EventArgs e)
        {
            //Limpio el listBox en caso se vuelva a dar click en el boton para no repetir lo mismo
            listBoxListado.Items.Clear();

            //Pongo visible la lista para mostrar los estudiantes por nombre - apellido
            listBoxListado.Visible = true;

            //Con el for recorro el arreglo y muestro cada uno de los items dentro
            for (int i = 0; i < Estudiante.Length; i++)
            {
                listBoxListado.Items.Add("Estudiante número " + (i + 1) + ": ");
                listBoxListado.Items.Add(Estudiante[i].Nombre + " " + Estudiante[i].Apellido);
            }
        }

        private void listBoxReporte_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Defino tamaño horizontal par poder mostrar todo los datos
            listBoxReporte.HorizontalExtent = 50;
        }

        private void listBoxListado_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Defino tamaño horizontal par poder mostrar todo los datos
            listBoxListado.HorizontalExtent = 50;
        }
    }
}
