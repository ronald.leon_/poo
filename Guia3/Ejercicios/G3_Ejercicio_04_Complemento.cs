﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicios
{
    public partial class G3_Ejercicio_04_Complemento : Form
    {
        public List<Persona> PersonaRecibe = null;
        public G3_Ejercicio_04_Complemento()
        {
            InitializeComponent();
        }
        private void actualizarGrid() //función que llena el DGV del formulario 2
        {
            //lo que tiene el atributo se lo doy al textBox
            dgvLlenado.DataSource = null;
            dgvLlenado.DataSource = PersonaRecibe;
        }

        private void btnLlenar_Click(object sender, EventArgs e)
        {
            actualizarGrid();
            //actualiza DGV cada vez que se presione.
        }

        private void txtFiltroNombre_TextChanged(object sender, EventArgs e)
        {
            //Como es en el evento TextChanged actualizo el datagrid para que descoloree si encuentra una coincidencia
            actualizarGrid();

            //Dos for each para cada fila y cada celda en esa fila
            foreach (DataGridViewRow fila in dgvLlenado.Rows)
            {
                foreach (DataGridViewCell celda in fila.Cells)
                {
                    //Captura el indice de la fila
                    int strFila = fila.Index;
                    //Obtengo el valor de ese indice
                    string Valor = Convert.ToString(fila.Cells[celda.ColumnIndex].Value);

                    //Si el valor de algun indice del datagrid coincide con lo puesto en el buscador
                    if (Valor == this.txtFiltroNombre.Text)
                    {
                        //Según el indice de la fila, columna donde coincida el dato, me coloreara esa celda
                        dgvLlenado.Rows[fila.Index].Cells[celda.ColumnIndex].Style.BackColor = Color.Aquamarine;
                                                
                        //Sintaxis para colorear toda la fila
                        //dgvLlenado.Rows[strFila].DefaultCellStyle.BackColor = Color.Aquamarine;
                    }
                }
            }
        }
    }
}
