﻿
namespace Ejercicios
{
    partial class G3_Ejercicio_02
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtNumEstudiantes = new System.Windows.Forms.TextBox();
            this.btnNumEstudiante = new System.Windows.Forms.Button();
            this.gboxMenu = new System.Windows.Forms.GroupBox();
            this.btnReporte = new System.Windows.Forms.Button();
            this.btnVerEstudiantes = new System.Windows.Forms.Button();
            this.btnNuevoEstudiante = new System.Windows.Forms.Button();
            this.gboxDatosEstudiantes = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnDatosEstudiantes = new System.Windows.Forms.Button();
            this.txtNota3 = new System.Windows.Forms.TextBox();
            this.txtNota2 = new System.Windows.Forms.TextBox();
            this.txtNota1 = new System.Windows.Forms.TextBox();
            this.txtMateria = new System.Windows.Forms.TextBox();
            this.txtApellido = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCarnet = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.listBoxListado = new System.Windows.Forms.ListBox();
            this.listBoxReporte = new System.Windows.Forms.ListBox();
            this.gboxMenu.SuspendLayout();
            this.gboxDatosEstudiantes.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(332, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ingrese número de estudiantes en su grupo";
            // 
            // txtNumEstudiantes
            // 
            this.txtNumEstudiantes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNumEstudiantes.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumEstudiantes.Location = new System.Drawing.Point(113, 65);
            this.txtNumEstudiantes.Name = "txtNumEstudiantes";
            this.txtNumEstudiantes.Size = new System.Drawing.Size(100, 21);
            this.txtNumEstudiantes.TabIndex = 1;
            this.txtNumEstudiantes.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnNumEstudiante
            // 
            this.btnNumEstudiante.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumEstudiante.Location = new System.Drawing.Point(104, 105);
            this.btnNumEstudiante.Name = "btnNumEstudiante";
            this.btnNumEstudiante.Size = new System.Drawing.Size(109, 31);
            this.btnNumEstudiante.TabIndex = 2;
            this.btnNumEstudiante.Text = "Continuar";
            this.btnNumEstudiante.UseVisualStyleBackColor = true;
            this.btnNumEstudiante.Click += new System.EventHandler(this.btnNumEstudiante_Click);
            // 
            // gboxMenu
            // 
            this.gboxMenu.Controls.Add(this.btnReporte);
            this.gboxMenu.Controls.Add(this.btnVerEstudiantes);
            this.gboxMenu.Controls.Add(this.btnNuevoEstudiante);
            this.gboxMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gboxMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gboxMenu.Location = new System.Drawing.Point(27, 172);
            this.gboxMenu.Name = "gboxMenu";
            this.gboxMenu.Size = new System.Drawing.Size(294, 156);
            this.gboxMenu.TabIndex = 3;
            this.gboxMenu.TabStop = false;
            this.gboxMenu.Text = "Menu";
            // 
            // btnReporte
            // 
            this.btnReporte.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReporte.Location = new System.Drawing.Point(18, 105);
            this.btnReporte.Name = "btnReporte";
            this.btnReporte.Size = new System.Drawing.Size(256, 31);
            this.btnReporte.TabIndex = 2;
            this.btnReporte.Text = "Ver reporte de estudiantes";
            this.btnReporte.UseVisualStyleBackColor = true;
            this.btnReporte.Click += new System.EventHandler(this.btnReporte_Click);
            // 
            // btnVerEstudiantes
            // 
            this.btnVerEstudiantes.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVerEstudiantes.Location = new System.Drawing.Point(18, 68);
            this.btnVerEstudiantes.Name = "btnVerEstudiantes";
            this.btnVerEstudiantes.Size = new System.Drawing.Size(256, 31);
            this.btnVerEstudiantes.TabIndex = 1;
            this.btnVerEstudiantes.Text = "Ver listado de estudiantes inscritos";
            this.btnVerEstudiantes.UseVisualStyleBackColor = true;
            this.btnVerEstudiantes.Click += new System.EventHandler(this.btnVerEstudiantes_Click);
            // 
            // btnNuevoEstudiante
            // 
            this.btnNuevoEstudiante.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNuevoEstudiante.Location = new System.Drawing.Point(18, 31);
            this.btnNuevoEstudiante.Name = "btnNuevoEstudiante";
            this.btnNuevoEstudiante.Size = new System.Drawing.Size(256, 31);
            this.btnNuevoEstudiante.TabIndex = 0;
            this.btnNuevoEstudiante.Text = "Ingresar nuevo estudiante";
            this.btnNuevoEstudiante.UseVisualStyleBackColor = true;
            this.btnNuevoEstudiante.Click += new System.EventHandler(this.btnNuevoEstudiante_Click);
            // 
            // gboxDatosEstudiantes
            // 
            this.gboxDatosEstudiantes.Controls.Add(this.label9);
            this.gboxDatosEstudiantes.Controls.Add(this.btnDatosEstudiantes);
            this.gboxDatosEstudiantes.Controls.Add(this.txtNota3);
            this.gboxDatosEstudiantes.Controls.Add(this.txtNota2);
            this.gboxDatosEstudiantes.Controls.Add(this.txtNota1);
            this.gboxDatosEstudiantes.Controls.Add(this.txtMateria);
            this.gboxDatosEstudiantes.Controls.Add(this.txtApellido);
            this.gboxDatosEstudiantes.Controls.Add(this.txtNombre);
            this.gboxDatosEstudiantes.Controls.Add(this.label8);
            this.gboxDatosEstudiantes.Controls.Add(this.label7);
            this.gboxDatosEstudiantes.Controls.Add(this.label6);
            this.gboxDatosEstudiantes.Controls.Add(this.label5);
            this.gboxDatosEstudiantes.Controls.Add(this.label4);
            this.gboxDatosEstudiantes.Controls.Add(this.label3);
            this.gboxDatosEstudiantes.Controls.Add(this.txtCarnet);
            this.gboxDatosEstudiantes.Controls.Add(this.label2);
            this.gboxDatosEstudiantes.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gboxDatosEstudiantes.Location = new System.Drawing.Point(424, 23);
            this.gboxDatosEstudiantes.Name = "gboxDatosEstudiantes";
            this.gboxDatosEstudiantes.Size = new System.Drawing.Size(250, 471);
            this.gboxDatosEstudiantes.TabIndex = 4;
            this.gboxDatosEstudiantes.TabStop = false;
            this.gboxDatosEstudiantes.Text = "Ingreso de datos";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(71, 43);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(104, 20);
            this.label9.TabIndex = 15;
            this.label9.Text = "Estudiante ";
            // 
            // btnDatosEstudiantes
            // 
            this.btnDatosEstudiantes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDatosEstudiantes.Location = new System.Drawing.Point(55, 418);
            this.btnDatosEstudiantes.Name = "btnDatosEstudiantes";
            this.btnDatosEstudiantes.Size = new System.Drawing.Size(111, 30);
            this.btnDatosEstudiantes.TabIndex = 14;
            this.btnDatosEstudiantes.Text = "Continuar";
            this.btnDatosEstudiantes.UseVisualStyleBackColor = true;
            this.btnDatosEstudiantes.Click += new System.EventHandler(this.btnDatosEstudiantes_Click);
            // 
            // txtNota3
            // 
            this.txtNota3.Location = new System.Drawing.Point(103, 370);
            this.txtNota3.Name = "txtNota3";
            this.txtNota3.Size = new System.Drawing.Size(121, 27);
            this.txtNota3.TabIndex = 13;
            this.txtNota3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtNota2
            // 
            this.txtNota2.Location = new System.Drawing.Point(103, 323);
            this.txtNota2.Name = "txtNota2";
            this.txtNota2.Size = new System.Drawing.Size(121, 27);
            this.txtNota2.TabIndex = 12;
            this.txtNota2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtNota1
            // 
            this.txtNota1.Location = new System.Drawing.Point(103, 275);
            this.txtNota1.Name = "txtNota1";
            this.txtNota1.Size = new System.Drawing.Size(121, 27);
            this.txtNota1.TabIndex = 11;
            this.txtNota1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtMateria
            // 
            this.txtMateria.Location = new System.Drawing.Point(103, 227);
            this.txtMateria.Name = "txtMateria";
            this.txtMateria.Size = new System.Drawing.Size(121, 27);
            this.txtMateria.TabIndex = 10;
            this.txtMateria.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtApellido
            // 
            this.txtApellido.Location = new System.Drawing.Point(103, 184);
            this.txtApellido.Name = "txtApellido";
            this.txtApellido.Size = new System.Drawing.Size(121, 27);
            this.txtApellido.TabIndex = 9;
            this.txtApellido.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(103, 136);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(121, 27);
            this.txtNombre.TabIndex = 8;
            this.txtNombre.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(21, 373);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 20);
            this.label8.TabIndex = 7;
            this.label8.Text = "Nota3:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(21, 326);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 20);
            this.label7.TabIndex = 6;
            this.label7.Text = "Nota2:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(21, 278);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 20);
            this.label6.TabIndex = 5;
            this.label6.Text = "Nota1:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 230);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Materia:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 191);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Apellido:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 139);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Nombre:";
            // 
            // txtCarnet
            // 
            this.txtCarnet.Location = new System.Drawing.Point(103, 84);
            this.txtCarnet.Name = "txtCarnet";
            this.txtCarnet.Size = new System.Drawing.Size(121, 27);
            this.txtCarnet.TabIndex = 1;
            this.txtCarnet.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "Carné:";
            // 
            // listBoxListado
            // 
            this.listBoxListado.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listBoxListado.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBoxListado.FormattingEnabled = true;
            this.listBoxListado.HorizontalScrollbar = true;
            this.listBoxListado.ItemHeight = 20;
            this.listBoxListado.Location = new System.Drawing.Point(27, 358);
            this.listBoxListado.Name = "listBoxListado";
            this.listBoxListado.ScrollAlwaysVisible = true;
            this.listBoxListado.Size = new System.Drawing.Size(294, 120);
            this.listBoxListado.TabIndex = 5;
            this.listBoxListado.SelectedIndexChanged += new System.EventHandler(this.listBoxListado_SelectedIndexChanged);
            // 
            // listBoxReporte
            // 
            this.listBoxReporte.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listBoxReporte.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBoxReporte.FormattingEnabled = true;
            this.listBoxReporte.HorizontalScrollbar = true;
            this.listBoxReporte.ItemHeight = 20;
            this.listBoxReporte.Location = new System.Drawing.Point(27, 515);
            this.listBoxReporte.Name = "listBoxReporte";
            this.listBoxReporte.ScrollAlwaysVisible = true;
            this.listBoxReporte.Size = new System.Drawing.Size(647, 120);
            this.listBoxReporte.TabIndex = 5;
            this.listBoxReporte.SelectedIndexChanged += new System.EventHandler(this.listBoxReporte_SelectedIndexChanged);
            // 
            // G3_Ejercicio_02
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(702, 652);
            this.Controls.Add(this.listBoxReporte);
            this.Controls.Add(this.listBoxListado);
            this.Controls.Add(this.gboxDatosEstudiantes);
            this.Controls.Add(this.gboxMenu);
            this.Controls.Add(this.btnNumEstudiante);
            this.Controls.Add(this.txtNumEstudiantes);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "G3_Ejercicio_02";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "G3_Ejercicio_02";
            this.gboxMenu.ResumeLayout(false);
            this.gboxDatosEstudiantes.ResumeLayout(false);
            this.gboxDatosEstudiantes.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNumEstudiantes;
        private System.Windows.Forms.Button btnNumEstudiante;
        private System.Windows.Forms.GroupBox gboxMenu;
        private System.Windows.Forms.Button btnVerEstudiantes;
        private System.Windows.Forms.Button btnNuevoEstudiante;
        private System.Windows.Forms.Button btnReporte;
        private System.Windows.Forms.GroupBox gboxDatosEstudiantes;
        private System.Windows.Forms.Button btnDatosEstudiantes;
        private System.Windows.Forms.TextBox txtNota3;
        private System.Windows.Forms.TextBox txtNota2;
        private System.Windows.Forms.TextBox txtNota1;
        private System.Windows.Forms.TextBox txtMateria;
        private System.Windows.Forms.TextBox txtApellido;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCarnet;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ListBox listBoxListado;
        private System.Windows.Forms.ListBox listBoxReporte;
    }
}