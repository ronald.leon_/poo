﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicios
{
    class Cliente
    {
        //Atributos
        private string numeroCuenta;
        private string nombre;
        private string apellido;
        private string dui;
        private string nit;
        private string tipoCuenta;
        private string monto;
        private string sucursal;

        //Métodos

        //Método para crear los correlativos de los números de cuenta
        public string generaNumeroCuenta(string tipoCuenta)
        {
            string correlativo;

            switch (tipoCuenta)
            {
                case "Cuenta corriente":
                    correlativo = "CC-";
                    break;
                
                case "Cuenta de ahorros":
                    correlativo = "CA-";
                    break;
                
                case "Cuenta a plazos":
                    correlativo = "CP-";
                    break;

                default:
                    correlativo = "";
                    break;
            }
            return correlativo + "0000";
        }


        public string NumeroCuenta
        {
            get { return numeroCuenta; }
            set { numeroCuenta = value; }
        }

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }

        public string Dui
        {
            get { return dui; }
            set { dui = value; }
        }

        public string Nit
        {
            get { return nit; }
            set { nit = value; }
        }

        public string TipoCuenta
        {
            get { return tipoCuenta; }
            set { tipoCuenta = value; }
        }

        public string Monto
        {
            get { return monto; }
            set { monto = value; }
        }

        public string Sucursal
        {
            get { return sucursal; }
            set { sucursal = value; }
        }
    }
}
