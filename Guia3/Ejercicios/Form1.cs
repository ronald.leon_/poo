﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicios
{
    public partial class FormMenu : Form
    {
        public FormMenu()
        {
            InitializeComponent();
        }

        private void btnEjercicio1_Click(object sender, EventArgs e)
        {
            G3_Ejercicio_01 Ejercicio1 = new G3_Ejercicio_01();
            Ejercicio1.Show();

        }

        private void btnEjercicio2_Click(object sender, EventArgs e)
        {
            G3_Ejercicio_02 Ejercicio2 = new G3_Ejercicio_02();
            Ejercicio2.Show();
        }

        private void btnEjercicio3_Click(object sender, EventArgs e)
        {
            G3_Ejercicio_03 Ejercicio3 = new G3_Ejercicio_03();
            Ejercicio3.Show();
        }

        private void btnEjercicio4_Click(object sender, EventArgs e)
        {
            G3_Ejercicio_04 Ejercicio4 = new G3_Ejercicio_04();
            Ejercicio4.Show();
        }
    }
}
