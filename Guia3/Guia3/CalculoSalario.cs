﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guia3
{
    public partial class CalculoSalario : Form
    {
        public CalculoSalario()
        {
            InitializeComponent();
        }

        Empleado miEmpleado = new Empleado(); //instancia de la clase Empleado
            Salario miSalario = new Salario(); //objeto de la clase Salario

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            /*Los valores obtenidos en los textbox son pasados a lo atributos por medio de sus propiedades,
            note que los mandamos a llamar a través de los objetos creados*/

            miEmpleado.Nombre = txtNombre.Text;
            miEmpleado.Identificacion = txtIdentificacion.Text;
            miEmpleado.SalarioDiario = Convert.ToDecimal(txtDiario.Text);
            miSalario.DiasLaborados = int.Parse(txtDiario.Text);
            MessageBox.Show("Datos ingresados con éxito");

        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            /*Enviaremos el valor del salario calculando al textbox respectivo, como es un dato
             númerico debemos convertirlo a texto, el cálculo lo hace el método de la clase salario*/
            txtSalario.Text = Convert.ToString(miSalario.CalcularSalario(miSalario.DiasLaborados,miEmpleado.SalarioDiario));
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            //limpiando todo para ingresar nuevos datos
            txtNombre.Clear();
            txtIdentificacion.Clear();
            txtDias.Clear();
            txtDiario.Clear();
            txtSalario.Clear();
            txtNombre.Focus(); //regresa el cursor al textbox del nombre
        }
    }
}
