﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guia3
{
    class Salario //publica para que pueda ser usada en cualquier instancia del proyecto
    {
        private int diasLaborados; //atributo
        public int DiasLaborados //propiedad del atributo
        { get; set; }


        //Método para calcular salarios
        public decimal CalcularSalario(int diasLab, decimal valordia)
        {
            decimal totalSalario = diasLab * valordia;
            return totalSalario;
        }
    }
}
